[![Docker Stars](https://img.shields.io/docker/stars/joshlyford/nginx-naxsi-builder.svg)](https://hub.docker.com/r/joshlyford/nginx-naxsi-builder/)
[![Docker Pulls](https://img.shields.io/docker/pulls/joshlyford/nginx-naxsi-builder.svg)](https://hub.docker.com/r/joshlyford/nginx-naxsi-builder/)
[![Docker Automated buil](https://img.shields.io/docker/automated/joshlyford/nginx-naxsi-builder.svg)](https://hub.docker.com/r/joshlyford/nginx-naxsi-builder/)

# Supported tags and respective `Dockerfile` links

-   [`1.17.3-0.56`, `mainline`, `latest` (*mainline/Dockerfile*)](https://github.com/joshlyford/nginx-naxsi-builder/blob/master/mainline/Dockerfile)
-   [`1.16.1-0.56`, `stable` (*stable/Dockerfile*)](https://github.com/joshlyford/nginx-naxsi-builder/blob/master/stable/Dockerfile)

# How to use this image

```console
$ docker run --name nginx-naxsi -p 80:80 \
    -v $(pwd):/usr/share/nginx/html -d dmgnx/nginx-naxsi
```

This will start a nginx service with default configuration, serving current working directory as your website.

# Volumes

-   `/etc/nginx/conf.d` : virtual hosts configuration
-   `/etc/nginx/naxsi` : your Naxsi rules
-   `/etc/nginx/ssl` : SSL certificates
-   `/usr/share/nginx/html` : web root directory
-   `/var/log/nginx` : log storage (redirected to the standard outputs by default)
